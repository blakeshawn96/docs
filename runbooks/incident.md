# Overview

This document serves as an action reference manual for Release Managers during incidents affecting availability of GitLab.com.

See the [availability] Handbook page for definitions of severities.

## Delivery Involvement

In high severity incidents, S2 and above, Release Managers should be involved if
our assistance is required for remediation.  Release Managers, nor the entirety
of the Delivery team have an on-call rotation as our services are normally
halted on non workdays.  Thus if an incident is spun up, consider reaching out
to an appropriate Delivery team member using their contact information in Slack.

If we are unable to be reached, continue reading through this document as this
is the same runbook that we would leverage for incidents.

## Decision Guidance

### Acceptable Remediation Options per Incident Level

| Available Remediation Option | Desired Incident Level Usage |
| ---------------------------- | ---------------------------- |
| Feature Flag changes         | All Severities               |
| Rollbacks                    | Evaluated for S2, Accepted for S1 |
| [Post-Deployment Patch]      | S1 **only**                  |

### Remediation Timings

* Feature Flags take less than 5 minutes to complete once the appropriate flag
  has been identified.
* Rolling back a deploy takes approximately 45 minutes.  If we need to perform
  the full rollback which includes Gitaly and Praefect, add an additional hour
  to the estimated time.  As noted in the [Deployment rollback] runbook, we do
  not automatically rollback Gitaly and Praefect.
* Hot Patch takes about 45 minutes to rollout once the appropriate patch has
  been identified and MR created.
* Merging a fix and deploying via the regular auto-deploy schedule has the least
  impact on other changes. After merging a fix will be available to deploy in
  ~5 hours.

### Remediation options

Consider the following options in combination with a [preferred timelines](#timeline):

1. **The source** of the problem **is unknown**
    * [Deployment rollback] - Consult the runbook for Rollbacks as there are
      limitations that may prevent this from being a viable option.
1. **The source** of the problem **is known**
    * Consult [feature flag log](https://gitlab.com/gitlab-com/gl-infra/feature-flag-log/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=host%3A%3Agitlab.com) and consider disabling any recently enabled features.
    * Revert the change that caused the issue and [pick the revert into the auto-deploy branch](./how_to_speed_auto_deploy_process_for_urgent_merge_requests.md). It is okay to tag immediately after picking without waiting for pipelines to pass on the alternative repository (likely on dev.gitlab.org) using the security release process pipeline override. If using this option, ensure that you confirm that all specs have passed prior to deploying to production.  This process shall be leveraged for incidents S2 and below.
    * Issue a [Post-Deployment Patch] (also commonly known as Hot Patch) to patch the production fleet. Post-deployment patches can only be applied when the fix is in the [Rails source code](https://gitlab.com/gitlab-org/gitlab).  This should be leveraged for only S1 incidents.

### Timeline

It is important to note that in situations such as this one, focus should be **exclusively** on reverting to the previously known working state.

Ensure that you challenge any decision that would consider creating the fix for the problem (if the fix ends up being broken, time was lost and now two problems exist). There are edge cases where it is okay to consider this, but those should be very rare.

In the first hour of the incident, it is common to consider the following options:

1. Disabling feature flags
1. Deployment rollback
1. Environment hot patch

Past the first hour, the options to consider are:

1. Reverting the offending code
1. Creating a new deployment

## Example S1 incident

As an example of S1 outage, we'll reference a scenario where post application deployment to GitLab.com, one of the more important workloads such as CI pipelines is no longer working for everyone on the platform.

At the start of the incident we would have:

1. Initiated incident in #incident-management Slack channel.
1. SRE on call, communications manager, current release manager, developer on call, and GitLab.com support member in the Incident Zoom room.

If any of the above are missing, ensure that you speak up and invite the missing people or create the missing resources.

As a release manager, your tasks would consist of providing sufficient background on tooling and changes that were introduced into the environment.

This means:

1. Provide the commit diff that was deployed to production.
    * Eg. Use `/chatops auto_deploy status` to find the latest branch running in production and link that to the developer on call for further investigation, or check the [grafana dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m) to find the latest deployed commit.
    * Note: This information is posted to all incident issues
1. Advise on the possible [remediation options](#remediation-options).
1. Define [a timeline](#timeline) for specific remediation options chosen in the incident discussion.
    * Eg. Think about what can be done immediately, what can be left for a couple of hours later, and what should be excluded from the conversation.

[availability]: https://about.gitlab.com/handbook/engineering/performance/#availability
[Deployment rollback]: ./rollback-a-deployment.md
[Post-Deployment Patch]: ../general/deploy/post-deployment-patches.md
[background database migrations]: ./background-migrations.md
